#!/usr/bin/env python3.4

from pyrser import grammar, meta
from pyrser.directives import ignore

class parserPseudoIni(grammar.Grammar):

	# Attributes
	entry = "Ini"
	grammar = """
	
 	Ini = [ [ Section:s #fill(_, s) ]+ eof ]
	Section = [ '[' id:i ']' [ ClefValeur:v #fill_section(_, i, v) ]+ ]
	ClefValeur = [ id:i '=' valeur:v #fill_dict(_, i, v) ]
	valeur = [ id | num | string ]

	"""

@meta.hook(parserPseudoIni)
def fill(self, ast, section):
	if not hasattr(ast, 'sections'):
		ast.sections = {}
	ast.sections[section.id] = section.value
	# print("fill")
	return True

@meta.hook(parserPseudoIni)
def fill_section(self, ast, id, value):
	ast.id = self.value(id);
	if not hasattr(ast, 'value'):
		ast.value = {}
	ast.value.update(value.dict)
	# print("fillsection")
	return True

@meta.hook(parserPseudoIni)
def fill_dict(self, ast, id, dict):
	ast.dict = {self.value(id):self.value(dict)}
	# print("fillvalue")
	return True
