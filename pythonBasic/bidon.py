#! /usr/bin/env python3.4

class Bidon:

	# Constructor
	def __init__(self, txt, num = 42, **kwvar):

		self.txt = txt
		self.num = num

		for key, value in kwvar.items():
			setattr(self, key, value)

	# Attributes
	zaz = "je suis un pro du python"


def var2listsort(*args):
	return sorted(args)
